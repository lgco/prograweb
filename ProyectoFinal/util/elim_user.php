<?php
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';
    
    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){

        require 'connection.php';
        
        #Validamos que el ID exista, para evitar manipulacion del link
        $usuario_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM usuarios WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $usuario_id);
        $stmt_val->execute();

        #Checamos el rowcount para ver si se ecnuentra algun usuario
        if( $stmt_val->rowCount()==0  ){
            #Si no existe el usuario, entonces regresamos a la pagina anterior
            echo "Usuario no encontrado";
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }

        #Preparamos un query para eliminar el user
        $sql = "DELETE FROM usuarios WHERE id=:id";
        $stmt_user = $conn->prepare($sql);
        $stmt_user->bindParam(':id', $usuario_id);


        #Tambien tenemos que eliminar las asignaciones de tareas de ese user para no dejarlas flotando
        #además, hay restricciones por llaves foraneas
        $sql2 = "DELETE FROM asignacion_tarea WHERE id_usuario=:id";
        $stmt_tarea = $conn->prepare($sql2);
        $stmt_tarea->bindParam(':id', $usuario_id);
        $stmt_tarea->execute();

        #Ya borrada la tarea, ahora si podemos borrar el user

        $stmt_user->execute();
        
        #Regresar a la pagina de usuarios con un mensaje
        echo '<script>window.location.href="../dashboard/admin_usuarios.php?mensaje=borrado";</script>';
    
    } 

    #Si no se tiene la info del ID simplemente regresamos a la pag anterior
    else {
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>