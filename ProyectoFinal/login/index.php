<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <script src="..\assets\js\bootstrap.bundle.min.js"></script>
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    
    <?php include '../includes/header1.php'?>

    <div class="bg-light w-100 p-3">
        <h1 class="text-center">Inicia Sesión</h1>

        <form action="index.php" method="POST">

            <div class="container w-50">
                <label for="email" class="fs-3 text-start">E-Mail</label>
                <br>
                <input type="email" class="form-control bg-secondary-subtle shadow" name="email" id="email" required>
            </div>

            <br>

            <div class="container w-50">
                <label for="password" class="fs-3 text-start">Contraseña</label>
                <br>
                <input type="password" class="form-control bg-secondary-subtle shadow" name="password" id="password" required>
            </div> 

            <br>

            <div class="container text-end w-50">
                <a href="registrar.php" class="link-opacity-50 link-opacity-75-hover">¿No estás registrado?</a>
                <br>
                <input type="submit" class="btn btn-primary shadow fs-4" value="Iniciar Sesión">
            </div> 

        </form>

        <?php   

            #Código para el inicio de sesión
                
        if(isset($_POST['email'], $_POST['password'])){
            if(!empty($_POST['email'] && !empty($_POST['password']))){
           
                $username = $_POST['email'];
                $password  = md5($_POST['password']);

                require '../util/connection.php';

                $query = "SELECT * FROM usuarios WHERE correo = :email AND contrasena = :password ";
                
                //Usaremos el prepare para preparar el query
                $resultado = $conn->prepare($query);

                //Con bindparam rellenamos los marcadores de posicion para los alias
                $resultado->bindParam(':email', $username, PDO::PARAM_STR);
                $resultado->bindParam(':password', $password, PDO::PARAM_STR);

                //Ejectuamos el query
                $resultado->execute();

                //Validamos que sí exista el usuario
                if($resultado->rowCount() > 0 ){
                    session_start();
                    $user = $resultado->fetch(PDO::FETCH_ASSOC);

                    #Asignamos variables de sesión
                    $_SESSION['user'] = $user['nombre'];
                    $_SESSION['rol'] = $user['id_rol'];
                    $_SESSION['id'] = $user['id'];

                    #Redireccionamos al menu principal
                    header("Location: ../dashboard/");
                }else{
                    #Mensaje en caso de que no exista el usuario o esté mal la contraseña
                    echo "<h3 class='error'>Credenciales no válidas!</h3>";
                }
            } else{
                    #Mensaje en caso de que haya datos vacíos
                echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
            }
        }

    ?>



    <p>Nota para el profe: Todas las contraseñas son 123</p>
    <p>Inicie sesion con la cuenta coordinador@gmail.com para acceso administrador </p>
    <p>Solo los administradores pueden agregar materias y tareas y asignarlas</p>

    </div>

    <?php
        include '../includes/footer.php';
    ?>   
</body>
</html>