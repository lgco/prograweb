<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 16 - SESSION / COOKIES</title>
    <link rel="stylesheet" type="text/css" href="default.css">
</head>
<body>
    <h2>Bienvenido</h2>
    <h4>Iniciar Sesión</h4>
    <form action="index.php" method="post">
        <label for="usuario">Ingresa tu Usuario: </label>
        <input type="text" name="usuario" id="usuario" required>
        <br>
        <label for="contra">Ingresa tu Contraseña: </label>
        <input type="password" name="contra" id="contra" required>
        <br>
        <label for="color">Elige el Color del Tema: </label>
        <input type="color" name="color" id="color" >
        <br>
        <input type="submit" value="Ingresar">
    </form>

    <?php
        ##Primera validacion ISSET
        if(ISSET($_POST['usuario'], $_POST['contra'])){
            #Segunda validación EMPTY
            if(!empty($_POST['usuario']) && !empty($_POST['contra'] == $pass)){
                $user = "Gerardo";
                $pass = "123";
                #Tercer validación CREDENCIALES
                if($_POST['usuario'] == $user && $_POST['contra']){
                    session_start();
                    #Variables de SESSION
                    $_SESSION['user'] = $_POST['usuario'];
                    $_SESSION['color'] = $_POST['color'];
                    header("Location: seleccion.php");
                }
                else{
                    echo "<h3>CREDENCIAS NO VÁLIDAS (Gerardo, 123)</h3>";
                }
            }
            else {
                echo "<h3>POR FAVOR, RELLENA TODOS LOS CAMPOS</h3>";
            }
            
        }
    ?>


</body>
</html>