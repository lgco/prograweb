<?php
    #Validaciones
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';

    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){
        require 'connection.php';

        #Validamos que el ID exista, para evitar manipulacion del link
        $materia_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM materias WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $materia_id);
        $stmt_val->execute();

        #Checamos el rowcount para ver si se ecnuentra alguna materia
        if( $stmt_val->rowCount()==0  ){
            #Si no existe , entonces regresamos a la pagina anterior
            echo "Materia no encontrada";
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }
        else{
            #asignamos los valores de la tarea encontrada a una variable
            $materia=$stmt_val->fetch(PDO::FETCH_ASSOC);
        }
    }
    else{
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Materia</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>
        <div class="bg-light w-100 p-3">

            <h1 class="text-center">Modificando Materia</h1>

        <form action="edit_materia.php?id=<?= $materia_id ?>" method="POST">
            <table class="table w-75">
                <tr>
                    <th>Nombre</th>
                    <th></th>
                </tr>

                <tr>
                    <td>
                        <input type="text" class="form-control bg-secondary-subtle shadow" name="nombre" id="nombre" value="<?= $materia['nombre'] ?>" required>
                    </td>
                    
                    <td>
                        <input type="submit" class="btn btn-success" value="Realizar ">
                    </td>

                </tr>
            </table>
        </form>

        <?php 
        if (empty($_POST)){
            exit;
         }
        #Código para la modificacion de materias
        if (isset($_POST['nombre']) && !empty($_POST['nombre']) )  {          
            $nombre = $_POST['nombre'];
            $query = "UPDATE materias SET nombre = :nombre WHERE id = :id_materia;";
            $resultado = $conn->prepare($query);
            $resultado->bindParam(':nombre', $nombre, PDO::PARAM_STR);
            $resultado->bindParam(':id_materia', $materia_id, PDO::PARAM_STR);
            $resultado->execute();
            #Regresar a la pagina de materias con un mensaje
            echo '<script>window.location.href="../dashboard/admin_materias.php?mensaje=editado";</script>';
        }  
        else {
            echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";

        }

        
        ?>

    </div>
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>




    