-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-05-2024 a las 07:55:41
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sist_tareas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_tarea`
--

CREATE TABLE `asignacion_tarea` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_tarea` int(11) DEFAULT NULL,
  `fecha_asignacion` timestamp NULL DEFAULT current_timestamp(),
  `id_estado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `asignacion_tarea`
--

INSERT INTO `asignacion_tarea` (`id`, `id_usuario`, `id_tarea`, `fecha_asignacion`, `id_estado`) VALUES
(61, 2, 16, '2024-05-19 04:49:42', 1),
(62, 19, 16, '2024-05-19 04:49:43', 1),
(63, 21, 16, '2024-05-19 04:49:44', 1),
(64, 22, 16, '2024-05-19 04:49:44', 1),
(65, 2, 19, '2024-05-19 04:49:55', 0),
(66, 19, 19, '2024-05-19 04:49:56', 0),
(67, 21, 19, '2024-05-19 04:49:56', 0),
(68, 22, 19, '2024-05-19 04:49:57', 0),
(69, 2, 17, '2024-05-19 04:50:06', 0),
(70, 19, 17, '2024-05-19 04:50:07', 0),
(71, 21, 17, '2024-05-19 04:50:07', 0),
(72, 22, 17, '2024-05-19 04:50:08', 0),
(73, 2, 20, '2024-05-19 05:22:46', 0),
(74, 19, 20, '2024-05-19 05:22:46', 0),
(75, 21, 20, '2024-05-19 05:22:47', 0),
(76, 22, 20, '2024-05-19 05:22:47', 0),
(77, 2, 21, '2024-05-19 05:23:52', 0),
(78, 19, 21, '2024-05-19 05:23:53', 0),
(79, 21, 21, '2024-05-19 05:23:53', 0),
(80, 22, 21, '2024-05-19 05:23:54', 0),
(81, 21, 22, '2024-05-19 05:25:15', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `nombre`) VALUES
(7, 'Programacion Web'),
(8, 'Adm Proyectos'),
(10, 'POO'),
(11, 'Big Data');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'admin'),
(0, 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `id` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `nivel_prioridad` tinyint(1) UNSIGNED NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_limite` timestamp NULL DEFAULT NULL,
  `id_usuario_creador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`id`, `id_materia`, `descripcion`, `nivel_prioridad`, `fecha_creacion`, `fecha_limite`, `id_usuario_creador`) VALUES
(16, 8, 'Haz un FODA', 1, '2024-05-19 03:55:35', '2024-05-19 05:59:00', 2),
(17, 8, 'Haz Otro FODA', 4, '2024-05-19 03:56:00', '2024-05-26 05:59:00', 2),
(19, 7, 'Haz tu proyecto', 5, '2024-05-19 04:24:08', '2024-05-20 05:59:00', 2),
(20, 10, 'Haz un JAVA', 6, '2024-05-19 05:22:21', '2024-05-25 05:59:00', 21),
(21, 11, 'Resuelve la economia mundial', 6, '2024-05-19 05:22:32', '2024-05-31 05:59:00', 21),
(22, 7, 'Defiende PHP', 9, '2024-05-19 05:25:11', '2024-05-26 05:59:00', 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `correo` varchar(60) NOT NULL,
  `id_rol` tinyint(1) NOT NULL DEFAULT 0,
  `contrasena` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `correo`, `id_rol`, `contrasena`) VALUES
(2, 'Gerardo Castañeda', 'gera@gmail.com', 1, '202cb962ac59075b964b07152d234b70'),
(19, 'Carlos Santillan', 'carlos@gmail.com', 0, '202cb962ac59075b964b07152d234b70'),
(21, 'Favián Flores', 'coordinador@gmail.com', 1, '202cb962ac59075b964b07152d234b70'),
(22, 'Juan Ochoa', 'juan@gmail.com', 0, '202cb962ac59075b964b07152d234b70');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asignacion_tarea`
--
ALTER TABLE `asignacion_tarea`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_tarea` (`id_tarea`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario_creador` (`id_usuario_creador`),
  ADD KEY `id_materia` (`id_materia`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asignacion_tarea`
--
ALTER TABLE `asignacion_tarea`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT de la tabla `materias`
--
ALTER TABLE `materias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asignacion_tarea`
--
ALTER TABLE `asignacion_tarea`
  ADD CONSTRAINT `asignacion_tarea_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asignacion_tarea_ibfk_4` FOREIGN KEY (`id_tarea`) REFERENCES `tareas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD CONSTRAINT `tareas_ibfk_1` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tareas_ibfk_2` FOREIGN KEY (`id_usuario_creador`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
