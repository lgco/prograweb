<?php

    $num1 = $_POST["num1"];
    $num2 = $_POST["num2"];
    $operacion = $_POST["operacion"];

    //Validacion de que no estén vacíos los datos
    if ($num1 == "" || $num2 == ""){
        echo "RELLENA LOS CAMPOS";
    }
    else{
        //Switch operación
        switch($operacion){
            case "suma":
                $suma = $num1 + $num2;
                echo " <br> La suma es: $suma";
                break;
            case "resta":
                $resta = $num1 - $num2;
                echo " <br> La resta es: $resta";
                break;
            case "divi":
                    $divi = $num1 / $num2;
                    echo " <br> La division es: $divi";
                    break;
            case "multi":
                    $multi = $num1 * $num2;
                    echo " <br> La multiplicacion es: $multi";
                    break;
            default:
                echo "ESA OPERACION NO EXISTE";
                break;
        }       
    }
?>