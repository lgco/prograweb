<?php
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';
    
    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){

        require 'connection.php';
        
        #Validamos que el ID exista, para evitar manipulacion del link
        $tarea_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM tareas WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $tarea_id);
        $stmt_val->execute();

        #Checamos el rowcount para ver si se ecnuentra algun usuario
        if( $stmt_val->rowCount()==0  ){
            #Si no existe la tarea, entonces regresamos a la pagina anterior
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }

        #Preparamos un query para eliminar la tarea
        $sql = "DELETE FROM tareas WHERE id=:id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $tarea_id);


        #Tambien tenemos que eliminar las asignaciones de tareas para no dejarlas flotando
        $sql2 = "DELETE FROM asignacion_tarea WHERE id_tarea=:id";
        $stmt_asignacion = $conn->prepare($sql2);
        $stmt_asignacion->bindParam(':id', $tarea_id);
        $stmt_asignacion->execute();

        #Ya borrada las asignaciones, ahora si podemos borrar el user

        $stmt->execute();
        
        #Regresar a la pagina de usuarios con un mensaje
        echo '<script>window.location.href="../dashboard/admin_tareas.php?mensaje=borrado";</script>';
    
    } 

    #Si no se tiene la info del ID simplemente regresamos a la pag anterior
    else {
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>