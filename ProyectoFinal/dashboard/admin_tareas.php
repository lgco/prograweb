<?php
    #Validación de usuario
    session_start();
    require '../includes/validate_session.php';
    #Validación de rol admin
    require '../includes/validate_admin.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Tareas</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>

    <div class="bg-light w-100 p-3">
        <h1 class="text-center">Listado de TODAS las tareas</h1>

        <?php
            #Por si recibimos un mensaje poder mostrarlo
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'completada'){
                echo "<h3 class='exito'>Tarea completada con éxito!</h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'descompletada'){
                echo "<h3 class='exito'>Tarea marcada como no completada! </h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'borrado'){
                echo "<h3 class='exito'>Tarea eliminada con éxito </h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'agregadp'){
                echo "<h3 class='exito'>Tarea agregada con éxito </h3>";
            }

            require '../util/connection.php';
            require '../util/functions.php';

            $sql = "SELECT * FROM tareas ORDER BY id";
            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            $resultado = $conn->query($sql);

            #Preparamos un query con las materias para poder desplegarlas 
            $sql2 = "SELECT * FROM materias ORDER BY id";
            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            $resultado2 = $conn->query($sql2);


     ?>

        <table class="table w-100">
                <tr>
                    <th>Id Tarea</th>                  
                    <th>Descripción</th>
                    <th>Id Materia</th>
                    <th>Nombre Materia</th>
                    <th># Usuarios Asignados</th>
                    <th>Nivel de Prioridad (1-10)</th>
                    <th>Fecha Creación</th>
                    <th>Fecha Límite</th>
                    <th>ID usuario creador</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>

                <?php
                //For each para iterar las tareas
                foreach($resultado as $registro) {
                        //For each para iterar los usuarios


                            ?>

                        <tr>
                            <td>
                                <?= $registro['id'] ?>
                            </td>
                            <td>
                                <?= $registro['descripcion'] ?>
                            </td>
                            <td>
                                <?= $registro['id_materia'] ?>
                            </td>
                            <td>
                                <?= 
                                nombreMateria($registro['id_materia']);
                                ?>
                            </td>
                            <td>
                                <?= cantidadAsignacionTarea($registro['id']) ?>
                            </td>
                            <td>
                                <?= $registro['nivel_prioridad'] ?>
                            </td>
                            
                            <td>
                                <?= $registro['fecha_creacion'] ?>
                            </td>
                            <td>
                                <?= $registro['fecha_limite'] ?>
                            </td>
                            <td>
                                <?= $registro['id_usuario_creador'] ?>
                            </td>

                            <td>
                                <a href="../util/elim_tarea.php?id=<?= $registro['id']; ?>">
                                    <button type="button" class="btn btn-danger">Eliminar</button>
                                </a>
                            </td>
                            <td>
                                <a href="../util/edit_tareas.php?id=<?= $registro['id']; ?>">
                                    <button type="button" class="btn btn-info">Editar </button>
                                </a>
                            </td>
                            <td>
                                <a href="../util/asignar_tarea.php?id=<?= $registro['id']; ?>">
                                    <button type="button" class="btn btn-secondary btn">Asignar</button>
                                </a>
                            </td>
                        </tr> 

                <?php } ?>
            
            </table>

            <h2>Añadir Nueva Tarea</h2>

            <form action="admin_tareas.php" method="POST" >
            <table class="table w-75">
                <tr>
                    <th>Descripcion</th>
                    <th>Materia</th>
                    <th>Nivel de Prioridad (1-10)</th>
                    <th>Fecha Limite</th>
                    <th></th>
                </tr>

                <tr>
                    <td>
                        <input type="text" class="form-control bg-secondary-subtle shadow" name="descripcion" id="descripcion" required>
                    </td>
                    
                    <td>
                    <select id="id_materia" name="id_materia" required>
                        
                        <?php 
                            foreach ($resultado2 as $materia){ ?>
                            <option value="<?=$materia['id']?>"> <?=$materia['nombre']?>  </option>                       
                        <?php } ?>
                    </select>
                    </td>

                    <td>
                    <select id="nivel_prioridad" name="nivel_prioridad">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                    </td>

                    <td>
                        <input type="date" class="form-control bg-secondary-subtle shadow" name="fecha_limite" id="fecha_limite" required>
                    </td>
                    
                    <td>
                        <input type="submit" class="btn btn-success" value="Realizar ">
                    </td>

                    
                </tr>
            </table>
            </form>

            <div class="row">
                <div class="col">
                    <a href="tareas.php">
                        <button type="button" class="btn btn-secondary btn-lg">Vista de Usuario</button>
                    </a>
            </div>
                                
        </div>
       
        <?php 
        echo '<br> <br>';
        include '../includes/volver.php';
        include '../includes/cerrar.php' ?>
    </div> 
    
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>


<?php 
    #Código para la modificacion de tareas
    if ( isset($_POST['descripcion'], $_POST['id_materia'], $_POST['nivel_prioridad'], $_POST['fecha_limite'] )) {

        if( !empty($_POST['descripcion']) && !empty($_POST['id_materia'])  && !empty($_POST['nivel_prioridad'])  && !empty($_POST['fecha_limite'])  ){
            $descripcion = $_POST['descripcion'];
            $id_materia = $_POST['id_materia'];                
            $prioridad = $_POST['nivel_prioridad'];
            $fecha_limite = $_POST['fecha_limite'];
            #convertimos la fecha limite a un timestamp
            $fecha_limite_timestamp = date('Y-m-d H:i:s', strtotime($fecha_limite . ' 23:59:00'));
                                
        
        #Validar id materia existente 

            #hacemos un query con las materias con ese id
            $materia_val = "SELECT * from materias where id = :id_materia";
            $materia_val = $conn->prepare($materia_val);
            $materia_val->bindParam(':id_materia', $id_materia, PDO::PARAM_STR);
            $materia_val->execute();

            #asociamos el resultado de la query con la variable
            $val = $materia_val->fetch(PDO::FETCH_ASSOC);

        #Revisar si existe la materia
        if($materia_val->rowCount()==0){  
            echo "<h3 class='error'>Esa materia no existe</h3>";
            exit;               
        }

        
        #Si el id es valido, ejecutamos todo esto.
        #Si fue invalido ya hubo un exit entonces esto no se ejecuta
        $query = "INSERT INTO tareas (id_materia, descripcion, nivel_prioridad, fecha_limite, id_usuario_creador) VALUE(:id_materia, :descripcion, :nivel_prioridad, :fecha_limite, :id_usuario)";
        $resultado = $conn->prepare($query);
        $resultado->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
        $resultado->bindParam(':id_materia', $id_materia, PDO::PARAM_STR);
        $resultado->bindParam(':nivel_prioridad', $prioridad, PDO::PARAM_STR);
        $resultado->bindParam(':fecha_limite', $fecha_limite_timestamp, PDO::PARAM_STR);
        $resultado->bindParam(':id_usuario', $_SESSION['id'], PDO::PARAM_STR);
        $resultado->execute();
        #Regresar a la pagina de tareas con un mensaje
        echo '<script>window.location.href="../dashboard/admin_tareas.php?mensaje=agregado";</script>';

    
        }
    else {
        echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
    }

    }
?>