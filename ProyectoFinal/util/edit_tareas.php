<?php
    #Validaciones
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';

    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){
        require 'connection.php';
        
        #Validamos que el ID exista, para evitar manipulacion del link
        $tarea_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM tareas WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $tarea_id);
        $stmt_val->execute();

        #Checamos el rowcount para ver si se ecnuentra algun usuario
        if( $stmt_val->rowCount()==0  ){
            #Si no existe el usuario, entonces regresamos a la pagina anterior
            echo "Tarea no encontrada";
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }
        else{
            #asignamos los valores de la tarea encontrada a una variable
            $tarea=$stmt_val->fetch(PDO::FETCH_ASSOC);
            #Preparamos un query con las materias para poder desplegarlas 
            $sql2 = "SELECT * FROM materias ORDER BY id";
            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            $resultado2 = $conn->query($sql2);
        }
    }
    else{
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Tarea</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>
        <div class="bg-light w-100 p-3">

            <h1 class="text-center">Modificando Tarea</h1>

        <form action="edit_tareas.php?id=<?= $tarea_id ?>" method="POST">
            <table class="table w-75">
                <tr>
                    <th>Descripcion</th>
                    <th>ID Materia</th>
                    <th>Prioridad</th>
                    <th>Nivel de Prioridad (1-10)</th>
                    <th></th>
                </tr>

                <tr>
                    <td>
                        <input type="text" class="form-control bg-secondary-subtle shadow" name="descripcion" id="descripcion" value="<?= $tarea['descripcion'] ?>" required>
                    </td>
                    
                    <td>
                        <select id="id_materia" name="id_materia" required>   
                            <?php 
                                foreach ($resultado2 as $materia){ ?>
                                        <option value="<?= $materia['id'] ?>" <?php if ($tarea['id_materia'] == $materia['id']) { ?> selected <?php } ?>><?= $materia['nombre'] ?></option>                       
                            <?php } ?>
                        </select>
                    </td>

                    <td>
                        <input type="number" class="form-control bg-secondary-subtle shadow" name="nivel_prioridad" id="nivel_prioridad" value="<?= $tarea['nivel_prioridad'] ?>" required>
                    </td>

                    <td>
                        <input type="date" class="form-control bg-secondary-subtle shadow" name="fecha_limite" id="fecha_limite" value="<?= $fecha_limite_formateada = date('Y-m-d', strtotime($tarea['fecha_limite']));?>" required>
                    </td>
                    
                    <td>
                        <input type="submit" class="btn btn-success" value="Realizar ">
                    </td>

                </tr>
            </table>
        </form>

        <?php 
        #Código para la modificacion de tareas
        

        if ( isset($_POST['descripcion'], $_POST['id_materia'], $_POST['nivel_prioridad'], $_POST['fecha_limite'] )) {

            if( !empty($_POST['descripcion']) && !empty($_POST['id_materia'])  && !empty($_POST['nivel_prioridad'])  && !empty($_POST['fecha_limite'])  ){
                $descripcion = $_POST['descripcion'];
                $id_materia = $_POST['id_materia'];                
                $prioridad = $_POST['nivel_prioridad'];
                $fecha_limite = $_POST['fecha_limite'];
                #convertimos la fecha limite a un timestamp
                $fecha_limite_timestamp = date('Y-m-d H:i:s', strtotime($fecha_limite . ' 23:59:00'));
                                 
            
            #Validar id materia existente 

                #hacemos un query con las materias con ese id
                $materia_val = "SELECT * from materias where id = :id_materia";
                $materia_val = $conn->prepare($materia_val);
                $materia_val->bindParam(':id_materia', $id_materia, PDO::PARAM_STR);
                $materia_val->execute();

                #asociamos el resultado de la query con la variable
                $val = $materia_val->fetch(PDO::FETCH_ASSOC);

            #Revisar si existe la materia
            if($materia_val->rowCount()==0){  
                echo "<h3 class='error'>Esa materia no existe</h3>";
                exit;               
            }

            
            #Si el id es valido, ejecutamos todo esto.
            #Si fue invalido ya hubo un exit entonces esto no se ejecuta
            $query = "UPDATE tareas SET descripcion = :descripcion, id_materia = :id_materia, nivel_prioridad = :nivel_prioridad, fecha_limite = :fecha_limite WHERE id = :id_tarea;";
            $resultado = $conn->prepare($query);
            $resultado->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
            $resultado->bindParam(':id_materia', $id_materia, PDO::PARAM_STR);
            $resultado->bindParam(':nivel_prioridad', $prioridad, PDO::PARAM_STR);
            $resultado->bindParam(':fecha_limite', $fecha_limite_timestamp, PDO::PARAM_STR);
            $resultado->bindParam(':id_tarea', $tarea_id, PDO::PARAM_STR);
            $resultado->execute();
            #Regresar a la pagina de tareas con un mensaje
            echo '<script>window.location.href="../dashboard/tareas.php?mensaje=editado";</script>';

        
            }
        else {
            echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
        }

        }
        ?>
    <a href="../dashboard/admin_tareas.php">
            <button type="button" class="btn btn-info btn-sm">Volver atrás</button>
    </a> 
    </div>
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>
