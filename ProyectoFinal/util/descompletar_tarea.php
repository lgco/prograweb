<?php
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';
    
    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){

        require 'connection.php';
        require 'functions.php';
        
        #Validamos que el ID de la tarea exista, para evitar manipulacion del link
        $tarea_id = $_GET['id'];
        
        if( checkTarea($_SESSION['id'], $tarea_id) == 'No' ){
            #Si no existe la tarea o no está asignada, entonces regresamos a la pagina anterior
            echo "Tarea no encontrado";
            exit;     
            echo '<script>window.history.go(-1);</script>';
        }

        #Preparamos un query para completar el user
        $sql = "UPDATE asignacion_tarea SET id_estado = 0 WHERE id_tarea=:id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $tarea_id);
        $stmt->execute();
        
        #Regresar a la pagina de usuarios con un mensaje

        echo '<script>window.location.href="../dashboard/tareas.php?mensaje=descompletada";</script>';
    
    } 

    #Si no se tiene la info del ID simplemente regresamos a la pag anterior
    else {
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>