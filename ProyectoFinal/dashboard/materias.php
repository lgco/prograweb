<?php
    #Validación de usuario
    session_start();
    require '../includes/validate_session.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Materias</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>

    <div class="bg-light w-100 p-3">
        <h1 class="text-center">Listado de Materias</h1>

        <?php
            #Por si recibimos un mensaje poder mostrarlo
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'borrado'){
                echo "<h3 class='exito'>Materia eliminada con éxito!</h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'agregado'){
                echo "<h3 class='exito'>Materia agregada con éxito!</h3>";
            }

            require '../util/connection.php';
            require '../util/functions.php';

            $sql = "SELECT * FROM materias ORDER BY id";
            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            $resultado = $conn->query($sql);

     ?>

        <table class="table w-75">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th></th>
                </tr>

                <?php
                //For each para iterar los usuarios
                foreach($resultado as $registro) { ?>

                        <tr>
                            <td>
                                <?= $registro['id'] ?>
                            </td>
                            <td>
                                <?= $registro['nombre'] ?>
                            </td>

                        </tr> 


            
            <?php } ?>  
            </table>
            
            <?php #Esta parte del código sólo se debe mostrar si el usuario es administrador
                if ($_SESSION['rol'] == 1){ ?>

                <div class="row">
                    <div class="col">
                        <a href="admin_materias.php">
                            <button type="button" class="btn btn-secondary btn-lg">Vista de Administrador</button>
                        </a>
                    </div>
                </div>

            <?php #Fin del código para administrador
                } ?>
            
        
        <?php
        echo '<br> <br>';
        include '../includes/volver.php';
        include '../includes/cerrar.php' ?>
    </div>          
    <?php
        include '../includes/footer.php';
    ?> 

</body>
</html>