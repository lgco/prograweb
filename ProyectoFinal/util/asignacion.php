
<?php 
        require 'connection.php';
        $user_id = $_GET['id_user'];
        $tarea_id = $_GET['tarea'];
        #Código para la modificacion de tareas
            $asignar = "INSERT INTO asignacion_tarea (id_usuario, id_tarea) VALUES (:user, :tarea)";
            $users = $conn->prepare($asignar);
            $users->bindParam(':user', $user_id);
            $users->bindParam(':tarea', $tarea_id);
            $users->execute();
            
            #Nos regresamos a la pagina para asignar tareas manteniendo la ID y agregando un mensaje
            #el mensaje lo tenemos que hacer un encode para que lo detecte bien
            echo "<script>window.location.href='../util/asignar_tarea.php?id=$tarea_id&mensaje=" . urlencode("completada") . "';</script>";


?>