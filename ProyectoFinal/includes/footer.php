
<footer class="bg-dark text-white text-center py-3">
    <div class="container">
        <p>&copy; <?php echo date("Y"); ?> Luis Gerardo Castañeda Ochoa.</p>
        <p>
            <a href="#" class="text-white">Programación Web</a> |
            <a href="https://www.cucea.udg.mx/" class="text-white">CUCEA</a>
        </p>
    </div>
</footer>
