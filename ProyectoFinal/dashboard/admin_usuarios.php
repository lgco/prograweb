<?php
    session_start();
    #Validación de usuario
    require '../includes/validate_session.php';
    #Validación de rol admin
    require '../includes/validate_admin.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administración de Usuarios</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">

</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>
    <div class="bg-light w-100 p-3">
        <h1 class="text-center">¿Qué deseas hacer?</h1>

        <?php
            #Por si recibimos un mensaje poder mostrarlo
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'borrado'){
                echo "<h3 class='exito'>Usuario eliminado con éxito!</h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'agregado'){
                echo "<h3 class='exito'>Usuario agregado con éxito!</h3>";
            }
            if(isset($_GET['editado']) && $_GET['editado'] === 'agregado'){
                echo "<h3 class='exito'>Usuario editado con éxito!</h3>";
            }

            require '../util/connection.php';
            require '../util/functions.php';
            $sql = "SELECT * FROM usuarios ORDER BY id";

            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            $resultado = $conn->query($sql);

            #Si se encuentra resultado, es decir, si sí existen usuarios
            if($resultado->rowCount() > 0){ ?>
            
            <h2>Administrar Usuarios</h2>

            <table class="table w-75">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Rol</th>
                    <th></th>
                    <th></th>
                </tr>

                <?php
                //For each para iterar los usuarios
                foreach($resultado as $registro) {?>

                <tr>
                    <td>
                        <?= $registro['id'] ?>
                    </td>
                    <td>
                        <?= $registro['nombre'] ?>
                    </td>
                    <td>
                        <?= $registro['correo'] ?>
                    </td>
                    <td>
                        <?= rolValidate($registro['id_rol']); ?>
                    </td>
                    <td>
                        <a href="../util/edit_user.php?id=<?= $registro['id']; ?>">
                            <button type="button" class="btn btn-info">Modificar</button>
                        </a>
                    </td>
                    <td>
                        <?php
                        #Evitar que un usuario pueda eliminarse a sí mismo 
                        if( $_SESSION['id'] != $registro['id'] ) { ?>
                            <a href="../util/elim_user.php?id=<?= $registro['id']; ?>">
                                <button type="button" class="btn btn-danger">Eliminar</button>
                            </a>
                        <?php } ?>
                        
                    </td>
                </tr> 
                <?php } ?>
            </table>

            <?php } ?>   
                
            <h2>Añadir Nuevo Usuario</h2>

            <form action="admin_usuarios.php" method="POST" >
                <table class="table w-75">
                    <tr>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Contraseña</th>
                        <th>Rol</th>
                        <th></th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>
                            <input type="text" class="form-control bg-secondary-subtle shadow" name="nombre" id="nombre" required>
                        </td>

                        <td>
                            <input type="text" class="form-control bg-secondary-subtle shadow" name="email" id="email" required>
                        </td>
                        
                        <td>
                            <input type="password" class="form-control bg-secondary-subtle shadow" name="password" id="password" required>
                        </td>

                        <td>
                            <select name="rol" id="rol">
                                <option value="0">Usuario</option>
                                <option value="1">Administrador</option> 
                            </select>
                        </td>

                        <td>
                            <input type="submit" class="btn btn-success" value="Registrar Usuario">
                        </td>

                    </tr>
                </table>
            </form>
            <?php 
        #Código para el registro de usuarios
        if(isset($_POST['nombre'], $_POST['email'], $_POST['password'])){
            if(!empty($_POST['nombre']) || !empty($_POST['email']) || !empty($_POST['password'])){
                $name = $_POST['nombre'];
                $email = $_POST['email'];
                $password  = md5($_POST['password']);
                $id_rol = $_POST['rol'];

                require '../util/connection.php';
            
            #Validar correo no duplicado
                $email_valid = "SELECT count(*) as count from usuarios where correo = :correo";
                $email_valid = $conn->prepare($email_valid);
                $email_valid->bindParam(':correo', $email, PDO::PARAM_STR);
                $email_valid->execute();
            #Asociando el resultado de la query con la variable
                $row = $email_valid->fetch(PDO::FETCH_ASSOC);

            #Revisar si está duplicado
            if($row['count'] > 0){
                echo "<h3 class='error'> Ese correo ya está registrado </h3>" ;

            }
            else{

                #Si el correo es valido, ejecutamos todo esto.
                    $query = "INSERT INTO usuarios (nombre, correo, contrasena, id_rol) VALUE(:nombre, :correo, :password, :id_rol)";
                    $resultado = $conn->prepare($query);
                    $resultado->bindParam(':nombre', $name, PDO::PARAM_STR);
                    $resultado->bindParam(':correo', $email, PDO::PARAM_STR);
                    $resultado->bindParam(':password', $password, PDO::PARAM_STR);
                    $resultado->bindParam(':id_rol', $id_rol, PDO::PARAM_STR);

                    $resultado->execute();

                    echo '<script>window.location.href="admin_usuarios.php?mensaje=agregado";</script>';


            }
        }
        else {
            echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
        }

        }
        echo '<br> <br>';
        include '../includes/volver.php';
        ?>

    </div>
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>



