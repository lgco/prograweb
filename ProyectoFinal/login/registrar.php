<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de Usuarios</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    
    <?php include '../includes/header1.php'?>

    <div class="bg-light w-100 p-3">
        <h1 class="text-center">Registra tus Datos</h1>

        <form action="registrar.php" method="POST">

             <div class="container w-50">
                <label for="nombre" class="fs-3 text-start">Ingresa tu Nombre: </label>
                <br>
                <input type="text" class="form-control bg-secondary-subtle shadow" name="nombre" id="nombre" required>
            </div>

            <br>

            <div class="container w-50">
                <label for="email" class="fs-3 text-start">Ingresa tu E-Mail: </label>
                <br>
                <input type="email" class="form-control bg-secondary-subtle shadow" name="email" id="email" required>
            </div>

            <br>

            <div class="container w-50">
                <label for="password" class="fs-3 text-start">Ingresa tu Contraseña</label>
                <br>
                <input type="password" class="form-control bg-secondary-subtle shadow" name="password" id="password" required>
            </div> 

            <br>

            <div class="container w-50">
                <label for="password2" class="fs-3 text-start">Repite tu Contraseña</label>
                <br>
                <input type="password" class="form-control bg-secondary-subtle shadow" name="password2" id="password2" required>
            </div> 

            <br>

            <div class="container text-end w-50">
                <a href="index.php" class="link-opacity-50 link-opacity-75-hover">¿Ya tienes cuenta?</a>
                <br>
                <input type="submit" class="btn btn-primary shadow fs-4" value="Registrarme">
            </div> 

        </form>


        <?php 
        #Código para el registro de usuarios

        #Se deja dentro del body para que los mensajes echo salgan dentro del body también 
        
        if(isset($_POST['nombre'], $_POST['email'], $_POST['password'], $_POST['password2'] )){
            if(!empty($_POST['nombre']) || !empty($_POST['email']) || !empty($_POST['password']) || !empty($_POST['password2'])){
                $name = $_POST['nombre'];
                $email = $_POST['email'];
                $password  = md5($_POST['password']);
                $password2  = md5($_POST['password2']);

                #Por default los usuarios serán tipo "user", es decir no admins
                $id_rol = 0;

                require '../util/connection.php';
            
            #Validar correo no duplicado
                $email_valid = "SELECT count(*) as count from usuarios where correo = :correo";
                $email_valid = $conn->prepare($email_valid);
                $email_valid->bindParam(':correo', $email, PDO::PARAM_STR);
                $email_valid->execute();
            #Asociando el resultado de la query con la variable
                $row = $email_valid->fetch(PDO::FETCH_ASSOC);

            #Revisar si está duplicado
            if($row['count'] > 0){
                echo "<h3 class='error'> Ese correo ya está registrado </h3>" ;

            }
            else{

                #Si el correo es valido, ejecutamos todo esto.

                #Validamos la contraseña que sean iguales
                if($password != $password2){
                    echo "<h3 class='error'> Las contraseñas no coinciden! </h3>" ;
                }
                
                else{
                    $query = "INSERT INTO usuarios (nombre, correo, contrasena, id_rol) VALUE(:nombre, :correo, :password, :id_rol)";
                    $resultado = $conn->prepare($query);
                    $resultado->bindParam(':nombre', $name, PDO::PARAM_STR);
                    $resultado->bindParam(':correo', $email, PDO::PARAM_STR);
                    $resultado->bindParam(':password', $password, PDO::PARAM_STR);
                    $resultado->bindParam(':id_rol', $id_rol, PDO::PARAM_STR);

                    $resultado->execute();
                    echo "<h3 class='exito'> Te has registrado correctamente! </h3>" ;
                }   

            }
            }
        else {
            echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
        }

        }

        

        ?>
    <a href="../login/">
        <button type="button" class="btn btn-info btn-sm">Ir a Inicio de Sesión</button>
    </a>
    </div>

    <?php
        include '../includes/footer.php';
    ?>   

</body>
</html>