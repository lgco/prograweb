<?php
    #Validación de usuario
    session_start();
    require '../includes/validate_session.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Tareas</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>

    <div class="bg-light w-100 p-3">
        <h1 class="text-center">Listado de tus tareas asignadas</h1>

        <?php
            #Por si recibimos un mensaje poder mostrarlo
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'completada'){
                echo "<h3 class='exito'>Tarea completada con éxito!</h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'descompletada'){
                echo "<h3 class='exito'>Tarea marcada como no completada! </h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'editado'){
                echo "<h3 class='exito'>Tarea editada con éxito! </h3>";
            }

            require '../util/connection.php';
            require '../util/functions.php';

            if(isset($_GET['order']) && $_GET['order'] == '1'){
                $sql = "SELECT * FROM tareas ORDER BY nivel_prioridad";
                $resultado = $conn->query($sql);
            }
            elseif(isset($_GET['order']) && $_GET['order'] == '2'){
                $sql = "SELECT * FROM tareas ORDER BY id_materia";
                $resultado = $conn->query($sql);
            }
            elseif(isset($_GET['order']) && $_GET['order'] == '3'){
                $sql = "SELECT * FROM tareas ORDER BY fecha_limite";
                $resultado = $conn->query($sql);
            }
            else{
                $sql = "SELECT * FROM tareas ORDER BY id";
                $resultado = $conn->query($sql);
            }
            
            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            

     ?>

        <table class="table w-100">
                <tr>
                    <th>Id</th>
                    <th>Materia</th>
                    <th>Descripción</th>
                    <th>Nivel de Prioridad (1-10)</th>
                    <th>Fecha Creación</th>
                    <th>Fecha Límite</th>
                    <th>Completada</th>
                    <th></th>
                    <th></th>
                </tr>

                <?php
                //For each para iterar los usuarios
                foreach($resultado as $registro) {

                        if( checkTarea($_SESSION['id'], $registro['id']) == 'Si' ){ ?>

                        <tr>
                            <td>
                                <?= $registro['id'] ?>
                            </td>
                            <td>
                                <?= 
                                nombreMateria($registro['id_materia']);
                                ?>
                            </td>
                            <td>
                                <?= $registro['descripcion'] ?>
                            </td>
                            <td>
                                <?= $registro['nivel_prioridad'] ?>
                            </td>
                            <td>
                                <?= $registro['fecha_creacion'] ?>
                            </td>
                            <td>
                                <?= $registro['fecha_limite'] ?>
                            </td>
                            <td>
                                <?= checkTareaCompletada($_SESSION['id'], $registro['id']) ?>
                            </td>
                            <td>
                                <a href="../util/completar_tarea.php?id=<?= $registro['id']; ?>">
                                    <button type="button" class="btn btn-success">Marcar como Completada</button>
                                </a>
                            </td>
                            <td>
                                <a href="../util/descompletar_tarea.php?id=<?= $registro['id']; ?>">
                                    <button type="button" class="btn btn-info">Marcar como No Completada</button>
                                </a>
                            </td>
                        </tr> 
                        <?php } ?>

                
            <?php } ?>  

            </table>

            <a href="tareas.php?order=1">
                <button type="button" class="btn btn-warning btn-sm">Ordenar por PRIORIDAD</button>
            </a>
            <a href="tareas.php?order=2">
                <button type="button" class="btn btn-warning btn-sm">Ordenar por MATERIAS</button>
            </a>
            <a href="tareas.php?order=3">
                <button type="button" class="btn btn-warning btn-sm">Ordenar por FECHA LIMITE</button>
            </a>
            <br>
            <br>


            <?php #Esta parte del código sólo se debe mostrar si el usuario es administrador
                if ($_SESSION['rol'] == 1){ ?>
                
                <div class="row">
                    <div class="col">
                        <a href="admin_tareas.php">
                            <button type="button" class="btn btn-secondary btn-lg">Vista de Administrador</button>
                        </a>
                    </div>
                </div>

            <?php #Fin del código para administrador
                } ?>
            
        
        <?php 

        echo '<br> <br>';
        include '../includes/volver.php';
        include '../includes/cerrar.php'; ?>
    </div>          
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>