<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 16 - SESSION / COOKIES</title>
    <link rel="stylesheet" type="text/css" href="style.php">
</head>
<body>
    <?php
        #Iniciar la sesión para poder usar variables de sesión
        session_start();
        echo "<h2> Bienvenido, ". $_SESSION['user'] . "</h2>";
        $metodo = $_POST['metodo'];

        #Dependiendo del metodo elegido se ejecuta uno u otro
        if($metodo == 'COOKIES'){
            echo "<p> Seleccionaste COOKIES <p>";
            #Checar si ya existe la cookie
            if(!isset($_COOKIE['visitas'])){
                #Si no, crearla y asignarle valor 2 (esta seria la primer visita)
                setcookie('visitas', 2 , time() + 3600);
                #Cuando se crea la cookie no se puede imprimir inmediatamente el valor, por eso aquí estará fijo
                echo "<p> Cantidad de visitas: 1 </p>";
            }
            else{
                #Si si existe, aumentarle 1 a su valor
                setcookie('visitas', $_COOKIE['visitas']+1 , time() + 3600);
                echo "<p> Cantidad de visitas: ". $_COOKIE['visitas'] . "</p>";
            }
            #Boton para salir
            echo "<br><br> <a href='salir.php'>Salir</a>";
        }
        elseif ($metodo == 'SESSION') {
            echo "<p> Seleccionaste SESSION <p>";
            #Validamos si ya existe la variable de sesión
            if(!isset($_SESSION['visitas'])){
                #Si no, crearla y asignarle valor 1
                $_SESSION['visitas'] = 1;
                echo "<p> Cantidad de visitas: ". $_SESSION['visitas'] . "</p>";
            }
            else{
                #Si si existe, aumentarle 1 a su valor
                $_SESSION['visitas'] = $_SESSION['visitas']+1;
                echo "<p> Cantidad de visitas: ". $_SESSION['visitas'] . "</p>";
            }
            #Boton para salir
            echo "<br><br> <a href='salir.php'>Salir</a>";
        }

        #Si a caso el método fuera distinto
        else{
            echo 'No deberías estar aquí...';
            header("Location: index.php");
        }
    ?>

</body>
</html>



