<?php
    #Validación de usuario
    session_start();
    require '../includes/validate_session.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu Princpial</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>

    <div class="bg-light w-100 p-3 h-100 ">
        <h1 class="text-center">¿Qué deseas hacer?</h1>

        <br>

         <div class="text-center ">

            <div class="row">
                <div class="col">
                    <a href="tareas.php">
                        <button type="button" class="btn btn-primary btn-lg w-75">Ver mis Tareas</button>
                    </a>
                </div>
                
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <a href="materias.php">
                        <button type="button" class="btn btn-primary btn-lg w-75">Listado de Materias</button>
                    </a>
                </div>
            </div>

            <br>

            <?php #Esta parte del código sólo se debe mostrar si el usuario es administrador
                if ($_SESSION['rol'] == 1){ ?>

                <div class="row">
                    <div class="col">
                        <a href="admin_usuarios.php">
                            <button type="button" class="btn btn-secondary btn-lg w-75">Administrar Usuarios</button>
                        </a>
                    </div>
                </div>

            <?php #Fin del código para administrador
                } ?>
            
        </div>
        <?php 
        include '../includes/cerrar.php'; 
        ?>
    </div>      
    
    <?php
        include '../includes/footer.php';
    ?>   
</body>
</html>