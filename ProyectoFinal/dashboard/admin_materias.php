<?php
    session_start();
    #Validación de usuario
    require '../includes/validate_session.php';
    #Validación de rol admin
    require '../includes/validate_admin.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Materias</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>

    <div class="bg-light w-100 p-3">
        <h1 class="text-center">Listado de Materias</h1>

        <?php
            #Por si recibimos un mensaje poder mostrarlo
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'borrado'){
                echo "<h3 class='exito'>Materia eliminada con éxito!</h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'agregado'){
                echo "<h3 class='exito'>Materia agregada con éxito!</h3>";
            }
            if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'editado'){
                echo "<h3 class='exito'>Materia editada con éxito!</h3>";
            }

            require '../util/connection.php';

            $sql = "SELECT * FROM materias ORDER BY id";

            //aqui no es necesario protegerse para inyeccion sql porque no se envia ningun dato
            $resultado = $conn->query($sql);
            ?>
            
            <h2>Administrar Materias</h2>

            <table class="table w-75">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th></th>
                    <th></th>
                </tr>

                <?php
                //For each para iterar las materias
                foreach($resultado as $registro) {?>

                <tr>
                    <td>
                        <?= $registro['id'] ?>
                    </td>
                    <td>
                        <?= $registro['nombre'] ?>
                    </td>

                    <td>
                        <a href="../util/edit_materia.php?id=<?= $registro['id']; ?>">
                            <button type="button" class="btn btn-info">Modificar</button>
                        </a>
                    </td>
                    <td>
                        <a href="../util/elim_materia.php?id=<?= $registro['id']; ?>">
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </a>
                    </td>
                </tr> 

                <?php } ?>
            </table>

            <h2>Añadir Nueva materia</h2>

            <form action="admin_materias.php" method="POST" >
            <table class="table w-75">
                <tr>
                    <th>Nombre</th>
                    <th></th>
                </tr>

                <tr>
                    <td>
                        <input type="text" class="form-control bg-secondary-subtle shadow" name="nombre" id="nombre" required>
                    </td>

                    <td>
                        <input type="submit" class="btn btn-success" value="Agregar ">
                    </td>

                </tr>
            </table>
            </form>
 

            <a href="materias.php">
                <button type="button" class="btn btn-secondary btn-lg">Vista de Usuario</button>
            </a>

    <?php
        echo '<br> <br>';
        include '../includes/volver.php';
    ?>
    </div>          
    <?php
        include '../includes/footer.php';
    ?>   
</body>
</html>


<?php 
        #Código para la insercion de materias
        if ( isset($_POST['nombre'])) {

            if( !empty($_POST['nombre'])){

                $nombre = $_POST['nombre'];
                                                                           
                $query = "INSERT INTO materias (nombre) VALUE(:nombre)";
                $resultado = $conn->prepare($query);
                $resultado->bindParam(':nombre', $nombre, PDO::PARAM_STR);

                $resultado->execute();
                #Regresar a la pagina de tareas con un mensaje
                echo '<script>window.location.href="../dashboard/admin_materias.php?mensaje=agregado";</script>';
       
            }
        else {
            echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
        }

        }
?>