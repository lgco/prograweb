
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 16 - SESSION / COOKIES</title>
    <link rel="stylesheet" type="text/css" href="style.php">
</head>
<body>
    <form action="main.php" method="post">
        <h2>Selecciona un Método</h2>
        <label for="cookies">COOKIES: </label>
        <input type="radio" name="metodo" id="cookies" value="COOKIES">
        <label for="session">SESSION: </label>
        <input type="radio" name="metodo" id="session" value="SESSION">
        <br><br>
        <input type="submit" value="ELEGIR">

    </form>
</body>
</html>