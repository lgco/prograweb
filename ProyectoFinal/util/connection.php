<?php
    $host = 'localhost'; //127.0.0.1
    $dbname = 'sist_tareas';
    $dbuser = 'root'; //usuario de la base de datos
    $dbpassword = '';

    try{
        //PDO es un objeto
        $conn = new PDO("mysql:host=$host; dbname=$dbname", $dbuser, $dbpassword);
        //echo "<h2> CONEXION EXITOSA </h2>";

            //PDOException captura la excepcion o error que arroje y la manda a la variable $e
    } catch(PDOException $e) {
        echo "<H2> ERROR EN LA CONEXION DE LA BASE DE DATOS: ". $e->getMessage() . "</H2>";
    }
    
?>