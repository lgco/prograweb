<?php
    #Validaciones
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';

    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){
        require 'connection.php';
        
        #Validamos que el ID exista, para evitar manipulacion del link
        $usuario_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM usuarios WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $usuario_id);
        $stmt_val->execute();

        #Checamos el rowcount para ver si se ecnuentra algun usuario
        if( $stmt_val->rowCount()==0  ){
            #Si no existe el usuario, entonces regresamos a la pagina anterior
            echo "Usuario no encontrado";
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }
        else{
            #asignamos los valores del usuario encontrado a una variable
            $usuario=$stmt_val->fetch(PDO::FETCH_ASSOC);
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Usuario</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>
        <div class="bg-light w-100 p-3">

            <h1 class="text-center">Modificando Usuario</h1>

        <form action="edit_user.php?id=<?= $usuario_id ?>" method="POST" >
            <table class="table w-75">
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Contraseña</th>
                    <th>Rol</th>
                    <th></th>
                    <th></th>
                </tr>

                <tr>
                    <td>
                        <!-- Input con valor actual como valor -->
                        <input type="text" class="form-control bg-secondary-subtle shadow" name="nombre" id="nombre" value="<?= $usuario['nombre']?>" required>
                    </td>

                    <td>
                        <input type="email" class="form-control bg-secondary-subtle shadow" name="email" id="email" value="<?= $usuario['correo'] ?>" required>
                    </td>
                    
                    <td>
                        <input type="password" class="form-control bg-secondary-subtle shadow" name="password" id="password" placeholder="***********">
                    </td>

                    <td>
                        <select name="rol" id="rol">

                            <?php 
                                #Para poner un select default, tenemos que saber que rol tiene el usuario y en base eso
                                #ocurre 1 de 2 escenarios para el codigo HTML con el valor SELECTED en el option correspondiente
                                if ($usuario['id_rol'] == 0){
                                    ?>
                                    <option value="0" selected>Usuario</option>
                                    <option value="1">Administrador</option> 

                                <?php }
                                else{
                                    ?>
                                    <option value="0" >Usuario</option>
                                    <option value="1" selected>Administrador</option> 

                                    <?php } ?>

                            </select>


                    </td>

                    <td>
                        <input type="submit" class="btn btn-success" value="Realizar Cambios">
                    </td>

                </tr>
            </table>
        </form>

        <?php 
        #Código para la modificacion de usuarios

        #Se deja dentro del body para que los mensajes echo salgan dentro del body también 
        #Validamos que no dejen un campo vacío, no importa si no se modifica alguno, pues igual quedara seteado con el valor anterior

        if ( isset($_POST['nombre'], $_POST['email'], $_POST['password'])){
            #No verificamos contraseña porque esa no estamos mostrando el valor anterior entonces si puede quedar vacía
            if( !empty($_POST['nombre']) || !empty($_POST['email'])){
                $name = $_POST['nombre'];
                $email = $_POST['email'];                
                $id_rol = $_POST['rol'];

                #Para la contraseña, si la dejamos vacia vamos a dejar el valor que se tenia antes
                if (empty($_POST['pasword'])){
                    $password = $usuario['contrasena'];
                }
                
                #Y si no está vacia, entonces asignamos el nuevo valor
                else{
                    $password  = md5($_POST['password']);
                }
                
            
            #Validar correo no duplicado
    
                #hacemos un query con los usuarios con ese correo
                $email_self = "SELECT * from usuarios where correo = :correo";
                $email_self = $conn->prepare($email_self);
                $email_self->bindParam(':correo', $email, PDO::PARAM_STR);
                $email_self->execute();

                #asociamos el resultado de la query con la variable
                $self = $email_self->fetch(PDO::FETCH_ASSOC);

            #Revisar si existe el correo en algun usuario
            if($email_self->rowCount()>0){
                #Revisamos si es el que ya tenia el usuario antes
                if( $self['id'] != $usuario_id ){
                    #si no es, entonces no permitimos que se cambie para no tener correos duplicados
                    echo "<h3 class='error'> Ese correo ya está registrado </h3>" ;
                    exit;
                }       
            }

    
            #Si el correo es valido, ejecutamos todo esto.
            #Si fue invalido ya hubo un exit entonces esto no se ejecuta
            $query = "UPDATE usuarios SET nombre = :nombre, correo = :correo, contrasena = :password, id_rol = :id_rol WHERE id = :id_usuario;";
            $resultado = $conn->prepare($query);
            $resultado->bindParam(':nombre', $name, PDO::PARAM_STR);
            $resultado->bindParam(':correo', $email, PDO::PARAM_STR);
            $resultado->bindParam(':password', $password, PDO::PARAM_STR);
            $resultado->bindParam(':id_rol', $id_rol, PDO::PARAM_STR);
            $resultado->bindParam(':id_usuario', $usuario_id, PDO::PARAM_STR);
            $resultado->execute();
            #Regresar a la pagina de usuarios con un mensaje
            echo '<script>window.location.href="../dashboard/admin_usuarios.php?mensaje=editado";</script>';

        
            }
        else {
            echo "<h3 class='error'> No puedes dejar campos vacios!</h3>";
        }

        }
        echo '<br> <br>';
        include '../includes/volver.php';
        ?>

    </div>
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>




    