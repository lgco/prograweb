<?php
    function rolValidate($rol){
        switch($rol){
            case 0:
                return 'Usuario';
            break;
            case 1:
                return 'Administrador';
            break;
        }
    }

    

    function checkTarea($id_user, $id_tarea){
        require 'connection.php';

        $sql = "SELECT * FROM asignacion_tarea where id_usuario = :id_usuario AND id_tarea = :id_tarea";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id_usuario', $id_user);
        $stmt->bindParam(':id_tarea', $id_tarea);
        $stmt->execute();

        switch($stmt->rowCount()){
            case 0:
                return 'No';
            break;
            default:
                return 'Si';
            break;
        }
    }

    function checkTareaCompletada($id_user, $id_tarea){
        require 'connection.php';

        $sql = "SELECT * FROM asignacion_tarea where id_usuario = :id_usuario AND id_tarea = :id_tarea AND id_estado = 1";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id_usuario', $id_user);
        $stmt->bindParam(':id_tarea', $id_tarea);
        $stmt->execute();
        

        switch($stmt->rowCount()){
            case 0:
                return 'No';
            break;

            default:
                return 'Si';
            break;
        }
    }

    function cantidadAsignacionTarea($id_tarea){
        require 'connection.php';
        $sql = "SELECT COUNT(*) as count from asignacion_tarea where id_tarea = :id_tarea";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id_tarea', $id_tarea);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result['count'];
    }

    function nombreMateria($id_materia){
        require 'connection.php';
        $sql = "SELECT * from materias WHERE id = :id_materia";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id_materia', $id_materia);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result['nombre'];

    }
?>