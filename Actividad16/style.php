<?php 
    #Codigo para poder usar variables de PHP en CSS
    header("Content-type: text/css; charset: UTF-8");
    #Iniciar sesion para usar el color
    session_start();

    #Asignacion del color a una variable para usarlo abajo

    #Validamos si ya se eligió el color, sino se usará un negro
    if(isset($_SESSION['color'])){
        $color = $_SESSION['color'];
    }
    else{
        $color = "#b87e14";
    }

?>
    p{
        font-family: 'Courier New', Courier, monospace;
        font-size: 24px;
        width: max-content;
        height: min-content;
        margin: auto;
        color: <?= $color ?>;
    }

    a{
        display: block;
        width: max-content;
        height: max-content;
        text-decoration: none;
        font-size: 24px;
        color: <?= $color ?>;
        border: solid 5px <?= $color ?>;
        border-radius: 10%;
        margin: auto;
        padding: 10px;
        font-weight: bold;
    }

    h2{
        font-size: 42px;
        font-family: 'Courier New', Courier, monospace;
        width: max-content;
        height: min-content;
        margin: auto;
        color: <?= $color ?>;
    }  
    form{
        font-size: 24px;
        width: max-content;
        height: min-content;
        text-align: center;
        margin: auto;
    }   


