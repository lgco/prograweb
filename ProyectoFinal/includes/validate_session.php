<?php
#Sirve para validar que haya sesion iniciada
    if(!isset($_SESSION['user'])){
        #Mandamos al login si no está iniciada
        header("Location: ../login/");
        exit;
    }
?>