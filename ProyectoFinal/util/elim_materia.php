<?php
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';
    
    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){

        require 'connection.php';
        
        #Validamos que el ID exista, para evitar manipulacion del link
        $materia_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM materias WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $materia_id);
        $stmt_val->execute();

        #Checamos el rowcount para ver si se ecnuentra alguna materia
        if( $stmt_val->rowCount()==0  ){
            #Si no existe la materia, entonces regresamos a la pagina anterior
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }

        #Preparamos un query para eliminar la materia
        $sql = "DELETE FROM materias WHERE id=:id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $materia_id);
        $stmt->execute();

        #No necesitamos borrar las asignaciones ni las tareas porque la base de datos funciona con restricciones ON CASCADE

        #Regresar a la pagina de materias con un mensaje
        echo '<script>window.location.href="../dashboard/admin_materias.php?mensaje=borrado";</script>';
    
    } 

    #Si no se tiene la info del ID simplemente regresamos a la pag anterior
    else {
        echo '<script>window.history.go(-1);</script>';
        exit;
    }

?>