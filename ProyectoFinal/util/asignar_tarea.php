<?php
    #Validaciones
    session_start();

    #Validación de usuario
    require '../includes/validate_session.php';

    #Validación de rol admin
    require '../includes/validate_admin.php';

    #Validar que se tenga la info del ID y que no este vacía
    if (isset($_GET['id']) && !empty($_GET['id'])){
        require 'connection.php';
        
        #Validamos que el ID exista, para evitar manipulacion del link
        $tarea_id = $_GET['id'];
        
        #Para ello prepararemos una consulta SQL
        $val = "SELECT * FROM tareas WHERE id=:id";
        $stmt_val = $conn->prepare($val);
        $stmt_val->bindParam(':id', $tarea_id);
        $stmt_val->execute();


        #Checamos el rowcount para ver si se ecnuentra alguna tarea
        if( $stmt_val->rowCount()==0  ){
            #Si no existe el usuario, entonces regresamos a la pagina anterior
            echo "Tarea no encontrada";
            echo '<script>window.history.go(-1);</script>';
            exit;       
        }
        else{
            #asignamos los valores de la tarea encontrada a una variable
            $tarea=$stmt_val->fetch(PDO::FETCH_ASSOC);
        }
        
    }
    else{
        if (empty($_GET['mensaje'])){
            echo '<script>window.history.go(-1);</script>';
            exit;
        }
        require 'connection.php';
    }

        

    #Creamos una variable con los usuarios
    $us = "SELECT * FROM usuarios";
    $usuarios= $conn->query($us);

    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asignar Tarea</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body class="w-75 h-100 m-auto bg-secondary">
    <?php include '../includes/header2.php'?>
        <div class="bg-light w-100 p-3">

            <h1 class="text-center">Modificando Tarea</h1>

        <form action="asignar_tarea.php?id=<?= $tarea_id ?>" method="POST">
            <table class="table w-75">
                <tr>
                    <th>Id Usuario</th>
                    <th>Nombre</th>
                    <th></th>
                </tr>

                <?php
                    if(isset($_GET['mensaje']) && $_GET['mensaje'] === 'completada'){
                        echo "<h3 class='exito'>Tarea asignada con éxito!</h3>";
                    }
                    //For each para iterar los usuarios
                    foreach($usuarios as $usuario) {
                            //For each para iterar los usuarios


                    ?>
                <tr>
                    <td>
                        <?= $usuario['id'] ?>
                    </td>
                        
                    <td>
                        <?= $usuario['nombre'] ?>
                    </td>
                    
                    <td>
                        <a href="../util/asignacion.php?id_user=<?= $usuario['id']; ?>&tarea=<?= $tarea_id; ?>">
                            <button type="button" class="btn btn-secondary btn">Asignar</button>
                        </a>
                    </td>
                </tr>

                
                
            <?php } ?>

            </table>
        </form>
        <a href="../dashboard/admin_tareas.php">
            <button type="button" class="btn btn-info btn-sm">Volver atrás</button>
        </a>               
        <?php 
        echo '<br> <br>';
        
        include '../includes/volver.php';
        include '../includes/cerrar.php' ?>


    </div>
    <?php
        include '../includes/footer.php';
    ?> 
</body>
</html>




    